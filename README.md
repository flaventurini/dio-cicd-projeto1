# Projeto 1 - CI/CD Pipeline - DIO

Projeto criado para desenvolver uma pipeline de CI/CD no GitLab para subir uma imagem Docker no servidor Cloud da GCP usando o GitLab Runner.

> Design da aplicação web: https://www.free-css.com/free-css-templates/page281/koppee

---
## Configuração GitLab Runner

Nas configurações do de CI/CD do repositório no Gitlab, expanda a parte onde está escrito `Runners` e siga as instruções de configuração do GitLab Runner:

> https://docs.gitlab.com/runner/install/

Para fazer essa configuração, eu criei uma instância de VM no Compute Engine da Google Cloud Platform direto da página/painel web do GCP. </br>

Após a criação da VM, eu acessei o SSH via Cloud Shell para configurar a máquina para rodar o GitLab Runner, usando os seguintes comando:

```bash
# Para atualizar os pacotes apt:
$ sudo su
$ apt-get update && apt-get upgrade -y

# Para instalar o GitLab Runner:
$ sudo apt-get install gitlab-runner

# Para registrar um Runner na VM:
$ sudo gitlab-runner register

# Respostas dadas após comando anterior:
$ https://gitlab.com/ # URL
$ # TOKEN DA MINHA CONTA -> está na página de configuração do CI/CD do GitLab
$ GCP RUNNER # Descrição
$ linux-vm, gcp-vm, cloud # tags que serão usadas na pipeline
$ shell # executor do runner
```

Se você atualizar a página de configuração do GitLab agora, vai perceber que existe um Specific Runner configurado.

---
## Configuração Docker e das variáveis do GitLab

Para conseguir rodar o Docker dentro da VM criada e usada anteriormente, será necessário instalar o Docker Engine:

```bash
# Instalação do Docker Engine:
$ sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
$ sudo mkdir -p /etc/apt/keyrings
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
$ echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
$ apt-get update
$ apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Configuração do Docker na VM:
$ sudo su
$ su gitlab-runner
$ docker login # informar o email e senha de login
$ exit
$ su gcp
$ docker login # informar o email e senha de login novamente

# Colocando o usuário ativo no GitLab e o gitlab-runner no grupo do Docker:
$ sudo usermod -aG docker gcp
$ sudo usermod -aG docker gitlab-runner

```

No painel do Compute Engine do GCP, clicar no nome da VM criada anteriormente e clicar em `Editar` no menu do topo. Rolando a página para baixo, na parte de `Segurança e acesso` adicione uma Chave Pública. Caso não saiba como criar uma via terminal, leia [esse site](https://support.google.com/youtube/answer/3071034?hl=pt-BR#zippy=%2Cpara-gerar-um-par-de-chaves-ssh-em-uma-m%C3%A1quina-com-macintosh-ou-linux).

Você precisará adicionar a chave privada, par dessa pública anteriormente criada (ambas são criadas juntas) em uma variável nas configurações do CI/CD do GitLab.
